# At Scale Packet Synthesizer for University of Oregon Netflow

The project should be compiled and run by the following steps:

1.  Clone the repository

2.  Change directory to "TimelySend" subdirectory for real-time packet synthesization (Test this directory)

3.  Issue "$ make" within the new directory via command line interface

4.  Setup a tcpdump in a new terminal to visualize functionality 

5.  Run the program via "$ sudo ./packetize [Netflow CSV File]"


Note that the "Netflow CSV File" should have the following sections delimited by commas:

1.  Start Timestamp in Epoch Format

2.  End Timestamp in Epoch Format

3.  Source IP Address

4.  Destination IP Address

5.  Source Port Number

6.  Destination Port Number

7.  IP Header Protocol Number

8.  Type of Service Value (from IP Header)

9.  TCP Flags

10.  Number of Packets

11.  Number of Bytes

12.  Router Ingress Port

13.  Router Egress Port

14.  Source Autonomous System

15.  Destination Autonomous System